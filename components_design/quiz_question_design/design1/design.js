
function Stopwatch(params) {
	this.stopwatchID = params.stopwatchID;
	this.id = null;
	this.interval = params.interval;
    this.ticks = params.ticks;
    this.maxTicks = params.ticks;
}

Stopwatch.prototype.decrement = function() {
 	(this.ticks > 0)
		this.ticks = this.ticks - 1;
}

Stopwatch.prototype.restart = function () {
    this.ticks = this.maxTicks;
    this.start();
}

Stopwatch.prototype.start = function() {

	var that = this;

	this.id = setInterval(function() {

        if(that.ticks == 0)
			that.stop();

        var angle = (that.maxTicks - that.ticks) * 360 / that.maxTicks;

        console.log("angle: ", angle);

        $(".stopwatch#" + that.stopwatchID + " .spinner.pie").css("transform", "rotate(" + angle + "deg)");

        if(angle > 180){
            $(".stopwatch .filler.pie").addClass("half");
            $(".stopwatch .mask").addClass("half");
        }
        else
        {
            $(".stopwatch .filler.pie").removeClass("half");
            $(".stopwatch .mask").removeClass("half");
        }

        that.decrement();


	}, this.interval);

}

Stopwatch.prototype.stop = function() {
	clearInterval(this.id);
	this.id = null;
}

var sw1 = new Stopwatch({interval: 20, ticks: 720, stopwatchID: 'sw1'});
sw1.start();

